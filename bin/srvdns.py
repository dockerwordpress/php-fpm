#!/usr/bin/python
from collections import namedtuple
import logging
from dns import resolver,ipv4
import socket


LOGGER = logging.getLogger(__name__)

SRV = namedtuple('SRV', ['host', 'port', 'priority', 'weight'])

res = resolver.Resolver()
res.nameservers = ['172.17.42.1']


class SRVQueryFailure(Exception):
    """Exception that is raised when the DNS query has failed."""
    def __str__(self):
        return 'SRV query failure: %s' % self.args[0]


def _query_srv_records(fqdn):

	try:
		
		return res.query(fqdn, 'SRV')
	except (resolver.NoAnswer,
			resolver.NoNameservers,
			resolver.NotAbsolute,
			resolver.NoRootSOA,
			resolver.NXDOMAIN) as error:
		LOGGER.error('Error querying SRV for %s: %r', fqdn, error)
		raise SRVQueryFailure(error.__class__.__name__)



def lookup(fqdn):

	answer = _query_srv_records(fqdn)
	return sorted([SRV(str(record.target).rstrip('.'),
					   record.port,
					   record.priority,
					   record.weight) for record in answer],
					key=lambda r: r.priority)




fpm_servers = lookup('d.devoply.local')
fpm_servers = lookup('w.devoply.local')


#wordpress.com.w.devoply.local 72.151.52.32 9000
#drupal.com.d.devoply.local

for server in fpm_servers:
	answer = res.query(server.host,'A')
	ip = answer.rrset.items[0].address
	print server.host, ip

