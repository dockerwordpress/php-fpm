#!/bin/bash
set -e
source /build/buildconfig
set -x

## Often used tools.
$minimal_apt_get_install curl wget less nano vim psmisc attr

## This tool runs a command as another user and sets $HOME.
cp /build/bin/setuser /sbin/setuser
cp /build/bin/etcdctl /usr/bin/etcdctl
cp /build/bin/skydnsset /usr/bin/skydnsset
cp /build/bin/wp-cli /usr/bin/wp
#cp /build/bin/fpm-config.sh /usr/local/bin/fpm-config.sh
cp /build/bin/rpcfpm /usr/sbin/rpcfpm


